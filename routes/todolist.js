const express = require("express");
const router = express.Router();
const TodolistController = require("../controllers/todolistController");

router.get("/todolists", TodolistController.findAll);
router.get("/todolists/:id", TodolistController.findOne);
router.post("/todolists", TodolistController.create);
router.delete("/todolists/:id", TodolistController.destroy);

module.exports = router;
