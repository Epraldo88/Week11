const express = require("express");
const router = express.Router();
const todolistRouter = require("./todolist");

router.use(todolistRouter);

module.exports = router;
