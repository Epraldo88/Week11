"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
	async up(queryInterface, Sequelize) {
		await queryInterface.bulkInsert("Todolists", [
			{
				title: "Belajar",
				status: "active",
				createdAt: new Date(),
				updatedAt: new Date(),
			},
			{
				title: "Kerja",
				status: "active",
				createdAt: new Date(),
				updatedAt: new Date(),
			},
			{
				title: "Valorant",
				status: "active",
				createdAt: new Date(),
				updatedAt: new Date(),
			},
		]);
		/**
		 * Add seed commands here.
		 *
		 * Example:
		 * await queryInterface.bulkInsert('People', [{
		 *   name: 'John Doe',
		 *   isBetaMember: false
		 * }], {});
		 */
	},

	async down(queryInterface, Sequelize) {
		await queryInterface.bulkDelete("Todolists", null, {});
		/**
		 * Add commands to revert seed here.
		 *
		 * Example:
		 * await queryInterface.bulkDelete('People', null, {});
		 */
	},
};
