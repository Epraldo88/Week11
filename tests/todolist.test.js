const app = require("../app");
const request = require("supertest");

describe("GET /todolists/:id", () => {
	it("test get /todolists", (done) => {
		request(app)
			.get("/todolists/1")
			.expect("Content-Type", /json/)
			.expect(200)
			.then((response) => {
				console.log(response.body);
				const firstData = response.body;
				expect(firstData.id).toBe(1);
				expect(firstData.title).toBe("Belajar");
				expect(firstData.status).toBe("active");
				done();
			})
			.catch(done);
	});

	it("test get /todolists/:id", (done) => {
		request(app)
			.get("/todolists/2")
			.expect("Content-Type", /json/)
			.expect(200)
			.then((response) => {
				console.log(response.body);
				const firstData = response.body;
				expect(firstData.id).toBe(2);
				expect(firstData.title).toBe("Kerja");
				expect(firstData.status).toBe("active");
				done();
			})
			.catch(done);
	});

	it("test post /todolists", (done) => {
		request(app)
			.post("/todolists")
			.send({ title: "Main Gitar", status: "active" })
			.set("Accept", "application/json")
			.expect("Content-Type", /json/)
			.expect(200)
			.then((response) => {
				const data = response.body;
				expect(data.title).toBe("Main Gitar");
				expect(data.status).toBe("active");
				done();
			})
			.catch(done);
	});

	it("delete post /todolists/:id", (done) => {
		request(app)
			.delete("/todolists/7")
			.expect("Content-Type", /json/)
			.expect(200)
			.then((response) => {
				const data = response.body;
				console.log(data);
				expect(data.message).toBe("Delete Successfully");
				done();
			})
			.catch(done);
	});
});
