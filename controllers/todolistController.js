const { Todolist } = require("../models");

class TodolistController {
	static findAll = async (req, res, next) => {
		try {
			const data = await Todolist.findAll({
				where: {
					status: "active",
				},
			});
			res.status(200).json(data);
		} catch (err) {
			next(err);
		}
	};

	static findOne = async (req, res, next) => {
		const { id } = req.params;
		try {
			const data = await Todolist.findOne({
				where: {
					id,
				},
			});
			if (data) {
				res.status(200).json(data);
			} else {
				throw { name: "ErrorNotFound" };
			}
		} catch (err) {
			next(err);
		}
	};

	static create = async (req, res, next) => {
		try {
			const { title, status } = req.body;
			const data = await Todolist.create({
				title,
				status,
			});

			res.status(200).json(data);
		} catch (err) {
			next(err);
		}
	};

	static destroy = async (req, res, next) => {
		try {
			const { id } = req.params;
			const data = await Todolist.update(
				{
					status: "inactive",
				},
				{
					where: {
						id,
					},
				}
			);
			if (data[0] === 1) {
				res.status(200).json({
					message: "Delete Successfully",
				});
			} else {
				throw { name: "ErrorNotFound" };
			}
		} catch (err) {
			next(err);
		}
	};
}

module.exports = TodolistController;
